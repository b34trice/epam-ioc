﻿using mef.Attributes;

namespace mef
{
    [ImportConstructor]
    public class ServiceConstructor
    {
        private readonly Logger _logger;

        public ServiceConstructor() { }

        public ServiceConstructor(Logger logger)
        {
            _logger = logger;
        }

        public bool IsNull()
        {
            return _logger == null;
        }
    }
}