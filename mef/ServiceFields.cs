﻿using mef.Attributes;

namespace mef
{
    public class ServiceFields
    {
        [Import]
        public Logger Logger { get; set; }

        [Import]
        public ServiceConstructor ser { get; set; }
    }
}
