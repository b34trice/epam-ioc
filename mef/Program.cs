﻿using System.Reflection;

namespace mef
{
    class Program
    {
        static void Main(string[] args)
        {
            var container = new Container();
            container.AddAssembly(Assembly.GetExecutingAssembly());

            var service = container.CreateInstance<ServiceFields>();
            var construct = container.CreateInstance<ServiceConstructor>();
            var logger = container.CreateInstance<Logger>();
            var servicei = container.CreateInstance<Service>();
        }
    }
}
