﻿using mef.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace mef
{
    public class Container
    {
        private readonly Dictionary<Type, object> _container;

        public Container()
        {
            _container = new Dictionary<Type, object>();
        }

        public void AddType(Type type)
        {
            if (_container.ContainsKey(type))
            {
                throw new ArgumentException("Type alredy exist");
            }

            Registration(type, Activator.CreateInstance(type));
        }

        public void AddAssembly(Assembly assembly)
        {
            foreach (var type in from item in assembly.DefinedTypes
                               .Where(item => item.CustomAttributes.Where(
                                   s => s.AttributeType.FullName == typeof(ImportAttribute).FullName ||
                                   s.AttributeType.FullName == typeof(ExportAttribute).FullName ||
                                   s.AttributeType.FullName == typeof(ImportConstructorAttribute).FullName).Any()
                                   || item.DeclaredProperties.Where(p => p.CustomAttributes.Any()).Any())
                               .Select(item => item)
                                 let type = item.AsType()
                                 select type)
            {
                Registration(type, Activator.CreateInstance(type, true));
            }
        }

        public T CreateInstance<T>() where T : class
        {
            return GetInstance(typeof(T)) as T;
        }

        public object CreateInstance(Type type)
        {
            return GetInstance(type);
        }

        private object GetInstance(Type type)
        {
            var result = _container[type];
            InitProperties(result);
            InitConstructors(result);
            return result;
        }

        private void InitProperties(object result)
        {
            foreach (var (dependency, instance) in from dependency in GetDependensies(result.GetType().GetProperties())
                                                   let instance = GetInstance(dependency.PropertyType)
                                                   select (dependency, instance))
            {
                dependency.SetValue(result, instance);
            }
        }

        private void InitConstructors(object result)
        {
            foreach (var (constructor, args, parameters) in from constructor in result.GetType().GetConstructors()
                                                            let args = new List<object>()
                                                            let parameters = constructor.GetParameters()
                                                            select (constructor, args, parameters))
            {
                if (!parameters.Any())
                {
                    continue;
                }

                args.AddRange(parameters.Select(parameter => GetInstance(parameter.ParameterType)));
                constructor.Invoke(args.ToArray());
            }
        }

        private List<PropertyInfo> GetDependensies(PropertyInfo[] properties)
        {
            return (from PropertyInfo property in properties
                    let attributes = property.PropertyType.CustomAttributes
                    where attributes.Where(a => a.AttributeType.FullName == typeof(ExportAttribute).FullName).Any()
                    select property).ToList();
        }

        private void Registration(Type type, object instance)
        {
            _container.Add(type, instance);
        }
    }
}